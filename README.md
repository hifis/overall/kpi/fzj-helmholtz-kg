# Service Usage

* FZJ  *Helmholtz KG*

## Plotting

* Plotting is be performed in the [Plotting project](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci).

## Data + Weighing

./stats/statistics.csv - schedule: weekly

Nginx logs are parsed to get the number of requests for the different services. Current data in the file is purely numerical, no weighing is applied. The CSV file is updated every week automatically by our system. The columns in the csv file:

- `Date` - date of the log entry for the last week
- `Search Index Page Requests` - total number of requests in a week for the index 
- `Search Requests` - total number of requests in a week for the search api
- `Sparql Index Page Requests` - total number of requests in a week for the index page of sparql service
- `Sparql Requests` - total number of requests in a week for the sparql service


## Schedule

* weekly
